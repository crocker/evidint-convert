# README #

A utility to convert a hex string to a base64 string.

### Quick Start ###

    Install SBT
    Clone this repo
    Build and test (2 options)
        sbt clean test
        sbt "run 45766964696e74"