name := "evidint-convert"

version := "1.0"

scalaVersion := "2.10.4"

libraryDependencies += "commons-codec" % "commons-codec" % "1.10"

libraryDependencies += "org.specs2" % "specs2-core_2.10" % "3.6.2"  % "test"

resolvers += "scalaz-bintray" at "http://dl.bintray.com/scalaz/releases"

scalacOptions in Test ++= Seq("-Yrangepos")

