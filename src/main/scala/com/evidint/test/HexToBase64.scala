package com.evidint.test

import org.apache.commons.codec.binary.{Base64, Hex}

/**
 * Converts a hex string to a base64 string.
 *
 * @author Jason Crocker
 */
object HexToBase64 {
  def main(args: Array[String]): Unit = {
    if (args.size != 1) {
      println("Please provide a hex input string.")
    }
    else {
      val output = HexToBase64.convert(args(0))
      println(output)
    }
  }

  def convert(input: String) : String = {
    val bytes = Hex.decodeHex(input.toCharArray)
    Base64.encodeBase64String(bytes)
  }
}
