package com.evidint.test

import org.specs2.mutable.Specification

class HexToBase64Test extends Specification {

  "HexToBase64" should {
    val input = "45766964696e74"
    val output = "RXZpZGludA=="

    "produce " + output + " when given an input of " + input in {

      HexToBase64.convert(input) mustEqual output
    }

  }

}